from flask import Flask, request, jsonify, Response
from flask_pymongo import PyMongo
from flask_restful import abort, Api, Resource
from bson import json_util
from bson.objectid import ObjectId

app = Flask(__name__)
app.config['MONGO_URI'] = "mongodb://db/test"
api = Api(app)
mongo = PyMongo(app)


def error_handler(message, status):
    response = jsonify({'messaage': message + " " + request.url,
                        'status': status})
    response.status_code = status
    return abort(response)


def abort_if_id_doesnt_exist(id):
    user = mongo.db.users.find({'_id': ObjectId(id)})
    if user is None:
        error_handler(id + " not found", 404)


def abort_if_id_exist(id):
    user = mongo.db.users.find({'_id': ObjectId(id)})
    if user is not None:
        error_handler(id + " already exist", 409)


def abort_if_user_id_exist(user_id):
    user = mongo.db.users.find_one({'user_id': user_id})
    if user is not None:
        error_handler(user_id + " already exist", 409)


def abort_if_user_id_doesnt_exist(user_id):
    user = mongo.db.users.find_one({'user_id': user_id})
    if user is None:
        error_handler(user_id + "doesn't exist", 404)


# Clase User
class User(Resource):
    def get(self, user_id):
        abort_if_user_id_doesnt_exist(user_id)

        user = mongo.db.users.find({'user_id': user_id})

        response = json_util.dumps(user)
        return Response(response, mimetype='application/json')

    def post(self):
        user_id = request.json['user_id']
        abort_if_user_id_exist(user_id)

        password = request.json['password']

        if user_id and password:
            mongo.db.users.insert_one({'user_id': user_id,
                                       'password': password})

            response = jsonify({'message': 'User has been created properly'})

            return response

        else:
            return error_handler("Resource doesn't exist", 409)

    def delete(self, user_id):
        abort_if_user_id_doesnt_exist(user_id)

        mongo.db.users.delete_many({'user_id': user_id})

        response = jsonify({'message': 'User has been deleted succesfully'})

        return response


# Clase Fichaje
class Fichaje(Resource):
    def get(self, user_id, fichaje_id):
        abort_if_id_doesnt_exist(user_id)
        # abort_if_id_doesnt_exist(fichaje_id)

        fichaje = mongo.db.users.find_one({'user_id': user_id,
                                           '_id': fichaje_id},
                                          {'user_id': 0,
                                           '_id': 0,
                                           })
        response = json_util.dumps(fichaje)
        return Response(response, mimetype='application/json')

    def post(self, user_id):
        # abort_if_user_id_doesnt_exist(user_id)

        date = request.json['date']
        tipo = request.json['tipo']

        # siendo pos :{'altitud':altitud,'latitud':"latitud}
        pos = request.json['pos']

        mongo.db.users.insert_one({'user_id': user_id,
                                   'date': date,
                                   'tipo': tipo,
                                   'pos': pos,
                                   })

        response = jsonify({'message': 'User was updated succesfully'})
        return response


# Prueba
@app.route('/')
def hello():
    return "Hola mundo"


api.add_resource(User, '/user', '/user/<string:user_id>')

api.add_resource(Fichaje, '/fichaje', '/user/<string:user_id>/fichaje/<ObjectId:fichaje_id>')

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
